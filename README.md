# Genshin Impact Characters

Download Genshin Impact characters' protraits from the [official website](https://ys.mihoyo.com/main/character/mondstadt?char=0).

![](./demo.jpg)

## How to install

The installation of Chrome is needed (used by `chromedriver.exe`)

Python >= 3.7

```shell
pip install -r requirements.txt
```

## How to use

```shell
python main.py
```

The downloaded images will be stored in the `./output` folder.
